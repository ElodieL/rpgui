import React, { useContext } from 'react';
import { CharacterContext } from '../contexts/characterContext/CharacterContext';

const EquipmentsPage = () => {
    const characterContext = useContext(CharacterContext);
    const all_equipments = characterContext.all_equipments;

    const style = {
        container: {
            diplay: 'flex',
            flexDirection: 'row',
            margin: '15px',

        }
    }

    return ( 
        <div style={style.container}>
            {console.log(all_equipments)}
            {all_equipments.map( equipment =>
                <div key={equipment.name}>
                    <h3>{equipment.name}</h3>
                    <p>type: {equipment.equipment_type}</p>
                    {equipment.stats.max_health === 0 ? '' : <p>vie: {equipment.stats.max_health}</p>}
                    {equipment.stats.max_mana === 0 ? '' : <p>mana: {equipment.stats.max_mana}</p>}
                    {equipment.stats.attack_damages === 0 ? '' : <p>dégâts physiques: {equipment.stats.attack_damages}</p>}
                    {equipment.stats.magic_damages === 0 ? '' : <p>dégâts magiques: {equipment.stats.magic_damages}</p>}
                    {equipment.stats.armor === 0 ? '' : <p>armure: {equipment.stats.armor}</p>}
                    {equipment.stats.magic_resist === 0 ? '' : <p>protection: {equipment.stats.magic_resist}</p>}
                    {equipment.stats.move_speed === 0 ? '' : <p>vitesse: {equipment.stats.move_speed}</p>}
                </div>
            )}

        </div>
     );
}
 
export default EquipmentsPage;