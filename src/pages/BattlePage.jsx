import React, { useContext } from 'react';
import { CharacterContext } from '../contexts/characterContext/CharacterContext';
import CharacterList from '../components/CharacterList';

const BattlePage = () => {
    const characterContext = useContext(CharacterContext);
    const all_characters = characterContext.all_characters;
    const playerTeam = characterContext.playerTeam;

    return ( 
        <div>
            <h1>BattlePage</h1>
            <CharacterList 
                characterList = {all_characters}
            />
            
            
            <CharacterList 
                characterList = {playerTeam}
            />




        </div>
     );
}
 
export default BattlePage;