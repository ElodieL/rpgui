export default class Stats {
    constructor(
        max_health,
        current_health,
        max_mana,
        current_mana,
        attack_damages,
        magic_damages,
        armor,
        magic_resist,
        move_speed
        ) {
            this.max_health = max_health;
            this.current_health = current_health;
            this.max_mana = max_mana;
            this.current_mana = current_mana;
            this.attack_damages = attack_damages;
            this.magic_damages = magic_damages;
            this.armor = armor;
            this.magic_resist = magic_resist;
            this.move_speed = move_speed;
    }

    //max health
    getMaxHealth() {
        return this.max_health;
    }
    setMaxHealth(max_health) {
        return this.max_health = max_health;
    }

    //current health
    getCurrentHealth() {
        return this.current_health;
    }
    setCurrentHealth(current_health) {
        return this.current_health = current_health;
    }

    //max mana
    getMaxMana() {
        return this.max_mana;
    }
    setMaxMana(max_mana) {
        return this.max_mana = max_mana;
    }

    //current mana
    getCurrentMana() {
        return this.current_mana;
    }
    setCurrentMana(current_mana) {
        return this.current_mana = current_mana;
    }

    //attack damages
    getAttackDamages() {
        return this.attack_damages;
    }
    setAttackDamages(attack_damages) {
        return this.attack_damages = attack_damages;
    }

    //magic damages
    getMagicDamages() {
        return this.magic_damages;
    }
    setMagicDamages(magic_damages) {
        return this.magic_damages = magic_damages;
    }

    //armor
    getArmor() {
        return this.armor;
    }
    setArmor(armor) {
        return this.armor = armor;
    }

    //magic resist
    getMagicResist() {
        return this.magic_resist;
    }
    setMagicResist(magic_resist) {
        return this.magic_resist = magic_resist;
    }

    //move speed
    getMoveSpeed() {
        return this.move_speed;
    }
    setMoveSpeed(move_speed) {
        return this.move_speed = move_speed;
    }
}