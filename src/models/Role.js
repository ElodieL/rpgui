export default class Role {
    constructor(type, name, picture, equipmentTypes_available, equipments_stater) {
        this.type = type;
        this.name = name;
        this.picture = picture
        this.equipmentTypes_available = equipmentTypes_available;
        this.equipments_stater = equipments_stater;
    }
}