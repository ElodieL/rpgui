export default class Equipments {
    constructor(head, arms, body, legs, feet, item, weapon, shield) {
        this.head = head;
        this.arms = arms;
        this.body = body;
        this.legs = legs;
        this.feet = feet;
        this.item = item;
        this.weapon = weapon; 
        this.shield = shield;
    }

    //Head
    getHead() {
        return this.head;
    }
    setHead(head) {
        return this.head = head;
    }

    //Arms
    getArms() {
        return this.arms;
    }
    setArms(arms) {
        return this.arms = arms
    }

    //body
    getBody() {
        return this.body;
    }
    setBody(body) {
        return this.body = body
    }

    //legs
    getLegs() {
        return this.legs;
    }
    setLegs(legs) {
        return this.legs = legs;
    }

    //feet
    getFeet() {
        return this.feet;
    }
    setFeet(feet) {
        return this.feet = feet;
    }

    //item
    getItem() {
        return this.item;
    }
    setItem(item) {
        return this.item = item;
    }

    //weapon
    getWeapon() {
        return this.weapon;
    }
    setWeapon(weapon) {
        return this.weapon = weapon;
    }

    //shield
    getShield() {
        return this.shield;
    }
    setShield(shield) {
        return this.shield = shield;
    }
}