export default class Race {
    constructor(type, name, stats, progressValues) {
        this.type = type;
        this.name = name;
        this.stats = stats;
        this.progressValues = progressValues;
    }
}