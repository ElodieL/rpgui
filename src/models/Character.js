import * as EQUIPMENT_TYPES from '../constants/EQUIPMENT_TYPES';
import nounchette from '../assets/img/nounchette.png';

export default class Character {
    constructor(
        id, 
        name, 
        picture,
        gender,
        race,
        role,
        lvl,
        exp_to_next_lvl, 
        current_exp,
        states, 
        stats,
        equipments, 
        ) {
            this.id = id;
            this.name = name;
            this.picture = picture;
            this.gender = gender;
            this.race = race;
            this.role = role;
            this.lvl = lvl;
            this.exp_to_next_lvl = exp_to_next_lvl;
            this.current_exp = current_exp;
            this.states = states;
            this.stats = stats;
            this.equipments = equipments;
    }

    initialiseNewCharacter(name, gender, race, role) {
        this.generateId();
        this.setName(name);
        this.setGender(gender);
        this.setRace(race);
        this.setRole(role);
        this.setStates([]);
        this.setPicture(role.picture);
        this.setLvl(1);
        this.setCurrentExp(0);
        this.setExpToNextLvl(100);
        this.setStats(race.stats);
        this.setEquipments(role.equipments_stater);
        return this;
    }
    
    //id
    generateId() {
        return this.id = '_' + Math.random().toString(36).substr(2, 9);
    }
    getId() {
        return this.id;
    }

    //name
    getName() {
        return this.name;
    }
    setName(name) {
        return this.name = name;
    }

    //picture
    getPicture() {
        return this.picture;
    }
    setPicture(picture) {
        return this.picture = picture;
    }
    setInitialPicture() {
        return this.picture = nounchette;
    }

    //gender
    getGender() {
        return this.gender;
    }
    setGender(gender) {
        return this.gender = gender;
    }

    //race
    getRace() {
        return this.race;
    }
    setRace(race) {
        return this.race = race;
    }

    //role
    getRole() {
        return this.role;
    }
    setRole(role) {
        return this.role = role;
    }

    //lvl
    getLvl() {
        return this.lvl;
    }
    setLvl(lvl) {
        return this.lvl = lvl;
    }
    levelUp() {
        this.lvl += 1;
        this.updateExpToNextlvl();
        this.updateStatsWithLevelUp()
    }

    //exp
    getCurrent_exp() {
        return this.current_exp;
    }
    setCurrentExp(current_exp) {
        return this.current_exp = current_exp;
    }
    earnExperience(exp_gain) {
        this.current_exp += exp_gain;
        while(this.current_exp >= this.exp_to_next_lvl) {
            if(this.current_exp === this.exp_to_next_lvl) {
            this.setCurrentExp(0);
            this.levelUp()

            } else if (this.current_exp > this.exp_to_next_lvl) {
                let exp = this.current_exp - this.exp_to_next_lvl;
                this.setCurrentExp(exp);
                this.levelUp()
            } 
        }
        return this;
    }

    //exp to next lvl
    getExpToNextLvl() {
        return this.exp_to_next_lvl;
    }
    setExpToNextLvl(exp_to_next_lvl) {
        return this.exp_to_next_lvl = exp_to_next_lvl;
    }
    updateExpToNextlvl() {
        this.exp_to_next_lvl = (this.exp_to_next_lvl + (this.exp_to_next_lvl/5)) * this.lvl;
        return this.exp_to_next_lvl;
    }

    //states
    getStates() {
        return this.states;
    }
    setStates(states) {
        return this.states = states;
    }
    addStates(state) {
        return this.states.push(state);
    }
    removeStates(state) {
        return console.log(state + 'enlevé');
    }

    //stats
    getStats() {
        return this.stats;
    }
    setStats(stats) {
        return this.stats = stats;
    }
    setStatsFromRace() {
        this.stats = this.race.stats;
    }
    updateStatsWithLevelUp() {
        if (this.lvl >= 1) {
            this.stats.setMaxHealth(this.stats.max_health + this.race.progressValues.health * this.lvl);
            this.stats.setCurrentHealth(this.stats.current_health + this.race.progressValues.health * this.lvl);
            this.stats.setMaxMana(this.stats.max_mana + this.race.progressValues.mana * this.lvl);
            this.stats.setCurrentMana(this.stats.current_mana + this.race.progressValues.mana * this.lvl);
            this.stats.setAttackDamages(this.stats.attack_damages + this.race.progressValues.attackDamages * this.lvl);
            this.stats.setMagicDamages(this.stats.magic_damages + this.race.progressValues.magicDamages * this.lvl);
            this.stats.setArmor(this.stats.armor + this.race.progressValues.armor * this.lvl);
            this.stats.setMagicResist(this.stats.magic_resist + this.race.progressValues.magicResist * this.lvl);
            this.stats.setMoveSpeed(this.stats.move_speed + this.race.progressValues.moveSpeed * this.lvl);
        }
        return this.stats;
    }
    

    //equipment
    getEquipments() {
        return this.equipments;
    }
    setEquipments(equipments) {
        return this.equipments = equipments;
    }
    setEquipmentStarterFromRole() {
        return this.equipments = this.role.equipments_stater;
    }
    wearAnEquipment(equipment) {
        switch(equipment.equipment_type) {
            case EQUIPMENT_TYPES.HEAD:
               this.equipments.setHead(equipment); 
               this.upgradeStatsFromEquipment(equipment);
               return this;
            case EQUIPMENT_TYPES.ARMS:
                this.equipments.setArms(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            case EQUIPMENT_TYPES.BODY:
                this.equipments.setBody(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            case EQUIPMENT_TYPES.LEGS:
                this.equipments.setLegs(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            case EQUIPMENT_TYPES.FEET:
                this.equipments.setFeet(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            case EQUIPMENT_TYPES.ITEM:
                this.equipments.setItem(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            case EQUIPMENT_TYPES.WEAPON:
                this.equipments.setWeapon(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            case EQUIPMENT_TYPES.SHIELD:
                this.equipments.setShield(equipment);
                this.upgradeStatsFromEquipment(equipment);
                return this;
            default:
                return this;
        }
    }
    upgradeStatsFromEquipment(equipment) {
        this.stats.setMaxHealth(this.stats.max_health + equipment.stats.max_health);
        this.stats.setCurrentHealth(this.stats.current_health + equipment.stats.current_health);
        this.stats.setMaxMana(this.stats.max_mana + equipment.stats.max_mana);
        this.stats.setCurrentMana(this.stats.current_mana + equipment.stats.current_mana);
        this.stats.setAttackDamages(this.stats.attack_damages + equipment.stats.attack_damages);
        this.stats.setMagicDamages(this.stats.magic_damages + equipment.stats.magic_damages);
        this.stats.setArmor(this.stats.armor + equipment.stats.armor);
        this.stats.setMagicResist(this.stats.magic_resist + equipment.stats.magic_resist);
        this.stats.setMoveSpeed(this.stats.move_speed + equipment.stats.move_speed);
        return this.stats;
    }
};