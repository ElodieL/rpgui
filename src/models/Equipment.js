export default class Equipment {
    constructor(equipment_type, name, stats, states, effects, abilities) {
        this.equipment_type = equipment_type;
        this.name = name;
        this.stats = stats;
        this.states = states;
        this.effects = effects;
        this.abilities = abilities;
    }

    createEquipment(type, name, stats, states, effects, abilities) {
        this.type = type;
        this.name = name;
        this.stats = stats;
        this.states = states;
        this.effects = effects;
        this.abilities = abilities;
        return this;
    }
}