import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import BattlePage from '../pages/BattlePage';
import CreateCharacterPage from '../pages/CreateCharacterPage';
import EquipmentsPage from '../pages/EquipmentsPage';

const Routing = () => {
    return ( 
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/battle" component={Battle}/>
                    <Route exact path="/createCharacter" component={CreateCharacter}/>
                    <Route exact path="/equipment" component={Equipments}/>
                </Switch>
            </Router>
        </div>          
     );
}
 

const Battle = () => {
    return <BattlePage/>
};


const CreateCharacter = () => {
    return <CreateCharacterPage/>

};

const Equipments = () => {
    return <EquipmentsPage/>
}

export default Routing;