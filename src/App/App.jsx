import React from 'react';
import './App.scss';
import { CharacterState } from '../contexts/characterContext/CharacterContext';
import Routing from './Routing';

function App() {
  return (
    <CharacterState>
      <Routing/>
    </CharacterState>
  );
}

export default App;
