import Equipment from "../models/Equipment";
import * as TYPES from '../constants/EQUIPMENT_TYPES';
import Stats from "../models/Stats";
import { createEquipment } from '../factories/equipmentFactory';


const iron_helmet = new Equipment(
    TYPES.HEAD, 
    'casque de fer', 
    new Stats(
       0, //max_health
       0, //current_health
       0, //max_mana
       0, //current_mana
       0,//attack_damages
       0, //magic_damages
       10, //armor
       0, //magic_resist
       0, //move_speed
    ),
    [],
    [],
);

const silk_gloves = new Equipment(
    TYPES.ARMS, 
    'gants de soie', 
    new Stats(
       0, //max_health
       0, //current_health
       5, //max_mana
       5, //current_mana
       0,//attack_damages
       0, //magic_damages
       0, //armor
       10, //magic_resist
       0, //move_speed
    ),
    [],
    [],
);

const purple_dress = new Equipment(
    TYPES.BODY, 
    'Robe violette', 
    new Stats(
       0, //max_health
       0, //current_health
       5, //max_mana
       5, //current_mana
       0,//attack_damages
       10, //magic_damages
       0, //armor
       5, //magic_resist
       0, //move_speed
    ),
    [],
    [],
);

export const tureen = createEquipment(TYPES.HEAD, 'soupière', 2, 0, 0, 0, 5, 2, 0, [], [], []);
export const pointed_hat = createEquipment(TYPES.HEAD, 'chapeau pointu', 0, 2, 0, 2, 2, 2, 0, [], [], []);
export const straw_hat = createEquipment(TYPES.HEAD, 'chapeau de paille', 0, 0, 0, 0, 3, 2, 0, [], [], []);

export const leather_shirt = createEquipment(TYPES.BODY, 'chemise de cuir', 2, 0, 0, 0, 5, 2, 0, [], [], []);

export const leather_gloves = createEquipment(TYPES.ARMS, 'gants de cuir', 0, 0, 0, 0, 2, 2 , 0, [], [], []);

export const wooden_sword = createEquipment(TYPES.WEAPON, 'épée courte', 0, 0, 5, 0, 0, 0, 0, [], [], []);
export const wooden_stick = createEquipment(TYPES.WEAPON, 'baguette de bois', 0, 0, 2, 8, 0, 0, 0, [], [], []);
export const wooden_bow = createEquipment(TYPES.WEAPON, 'arc de bois', 0, 0, 0, 5, 0, 0, 0, [], [], []);

export const HELMET = [
    tureen,
    iron_helmet
]

export const HAT = [
    pointed_hat,
    straw_hat,
]

export const GLOVES = [
    silk_gloves,
    leather_gloves
]

export const DRESS = [
    purple_dress
]

export const ARMOR = [
    leather_shirt,
]

export const SWORD = [
    wooden_sword
]

export const STICK = [
     wooden_stick,
 ]

 export const BOW = [
     wooden_bow
 ]


export const all_equipments = [
    iron_helmet,
    silk_gloves,
    purple_dress,
    wooden_sword,
    wooden_stick,
    wooden_bow,
    tureen,
    pointed_hat,
    straw_hat,
    leather_shirt,
    leather_gloves,
]