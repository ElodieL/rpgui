import * as RACE from '../constants/RACES';
import Race from './../models/Race';
import Stats from '../models/Stats';

const elf_stats = new Stats(
    100, //max_health + 2
    100, //current_health 
    50, //max_mana + 3
    50, //current_mana
    15, //attack_damages + 2
    30, //magic_damages + 4
    10, //armor + 1
    10, //magic_resist + 1
    15, //move_speed + 1
);

const human_stats = new Stats(
    115, //max_health + 3
    115, //current_health 
    35, //max_mana + 2
    35, //current_mana
    20, //attack_damages + 3
    20, //magic_damages + 3
    15, //armor + 2
    10, //magic_resist + 2
    10, //move_speed + 1
);

const ent_stats = new Stats(
    150, //max_health + 4
    150, //current_health
    20, //max_mana + 1
    20, //current_mana
    15, //attack_damages + 2
    15, //magic_damages + 2
    30, //armor + 3
    25, //magic_resist + 3
    5, //move_speed + 1
);

export const elf = new Race(
    RACE.ELF, 
    'elf', 
    elf_stats,
    {
        health: 2,
        mana: 3,
        attackDamages: 2,
        magicDamages: 4,
        armor: 1,
        magicResist: 1,
        moveSpeed: 1,
    }
);

export const human = new Race(
    RACE.HUMAN, 
    'humain', 
    human_stats,
    {
        health: 3,
        mana: 2,
        attackDamages: 3,
        magicDamages: 3,
        armor: 2,
        magicResist: 2,
        moveSpeed: 1,
    }
);

export const ent = new Race(
    RACE.ENT, 
    'Ent', 
    ent_stats,
    {
        health: 4,
        mana: 1,
        attackDamages: 2,
        magicDamages: 2,
        armor: 3,
        magicResist: 3,
        moveSpeed: 1,
    }
);