import Role from '../models/Role';
import * as ROLES from '../constants/ROLES'
import Equipments from '../models/Equipments';
import * as equipments from '../data/equipments';
import nounchette from '../assets/img/nounchette.png'

export const EMPTY = 'EMPTY';

let starter_warrior = new Equipments(
    EMPTY, 
    equipments.leather_gloves,
    equipments.leather_shirt,
);
let starter_mage = new Equipments(
    equipments.pointed_hat,
    equipments.leather_gloves,
    equipments.leather_shirt,
);
let starter_pyroman = new Equipments(
    equipments.straw_hat,
    equipments.leather_gloves,
    equipments.leather_shirt,
);
let starter_marksman = new Equipments(
    equipments.straw_hat,
    equipments.leather_gloves,
    equipments.leather_shirt,
);

export const warrior = new Role(
    ROLES.WARRIOR, 
    'guerrier',
    nounchette, 
    [],
    starter_warrior,
);
export const mage = new Role(
    ROLES.MAGE, 
    'mage', 
    nounchette, 
    [], 
    starter_mage
);
export const marksman = new Role(
    ROLES.MARKSMAN,
    'archer',
    nounchette,
    [],
    starter_marksman
)
export const pyroman = new Role(
    ROLES.PYROMAN, 
    'pyromane', 
    nounchette, 
    [], 
    starter_pyroman
);

