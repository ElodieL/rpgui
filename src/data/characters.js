import { createNewCharacter, createCharacter } from '../factories/CharacterFactory';
import * as races from '../data/races';
import * as roles from '../data/roles';
import * as GENDERS from '../constants/GENDERS';

export const eddy = createNewCharacter('Eddy', races.elf, GENDERS.FEMALE, roles.mage);
export const toto = createNewCharacter('Toto', races.human, GENDERS.MALE, roles.warrior);
export const normy = createNewCharacter('Normy', races.ent, GENDERS.FEMALE, roles.pyroman);

export const robert = createCharacter('Robert', GENDERS.MALE, races.human, roles.warrior);
export const hector = createCharacter ('Hector', GENDERS.MALE, races.elf, roles.mage);
export const martine = createCharacter('Martine', GENDERS.FEMALE, races.ent, roles.pyroman);
export const ludovic = createCharacter('Ludovic', GENDERS.MALE, races.elf, roles.marksman);

export const all_characters = [
  eddy,
  toto,
  normy,
  robert,
  hector,
  martine,
  ludovic
]

