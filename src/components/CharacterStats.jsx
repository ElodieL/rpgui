import React, { useState } from 'react';
import * as GENDERS from '../constants/GENDERS';
import StatBar from './StatBar';

const style = {
    container: {
        border: '1px solid black',
        borderRadius: '15px',
        margin: '10px',
        padding: '10px',
    },
    flexRow: {
        display : 'flex',
        flexDirection: 'row',
        itemsAlign: 'center',
        justifyContent: 'space-between'
    },
    title: {
        textAlign: 'center',
    },
    picture: {
        height: '100px',
        textAlign: 'center',
        diplay: 'inline',
        border: '1px solid black',
        borderRadius: '15px',
        marginRight: '5px'
    },
}

const CharacterStats = props => {
    const [isStatsVisible, setStatsIsVisible] = useState(false);

    const showStats = isStatsVisible => {
        return setStatsIsVisible(isStatsVisible = !isStatsVisible);
    }

    return (
        <div style={style.container}>
            <header>
                <div style={style.flexRow}>
                    <img 
                        style={style.picture} 
                        src={props.picture} alt=""
                        onClick={() => showStats(isStatsVisible)}
                    />
                    <div>
                        <h3>{props.name} {props.gender === GENDERS.FEMALE ? '♀' : '♂' }</h3>
                        <p>{props.role} lvl: {props.lvl}</p>
                    </div>
                </div>
                <ul>
                    <li>altérations d'état: {props.states}</li>
                    <li>
                        <StatBar
                            color = {'grey'}
                            stat_label = {'expérience'}
                            current_value = {props.current_exp}
                            max_value = {props.exp_to_next_lvl}
                        />
                    </li>
                    <li>
                        <StatBar
                            color = {'green'}
                            stat_label = {'vie'}
                            current_value = {props.current_health}
                            max_value = {props.max_health}
                        />
                    </li>
                    <li>
                        <StatBar
                            color = {'blue'}
                            stat_label = {'mana'}
                            current_value = {props.current_mana}
                            max_value = {props.max_mana}
                        />
                    </li>
                </ul>
            </header>
            {isStatsVisible ? 
                <main>
                    <h4>statistiques:</h4>
                    <ul>
                        <li>dégâts physiques : {props.attack_damages}</li>
                        <li>dégats magiques : {props.magic_damages}</li>
                        <li>armure : {props.armor}</li>
                        <li>protection : {props.magic_resist}</li>
                        <li>vitesse : {props.move_speed}</li>
                    </ul>
                    <h4>Equipements:</h4>
                    <ul>
                        <li>tête: {props.head}</li>
                        <li>corps: {props.body}</li>
                        <li>bras: {props.arms}</li>
                    </ul>
                    {props.footer}
                </main> 
                : ''
            }
            
        </div>
     );
}
export default CharacterStats;