import React from 'react';
import CharacterStats from './CharacterStats';
import Button from './button';
import { EMPTY } from '../data/roles';

const CharacterList = props => {

    const style = {
        container: {
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
        }
    }

    return ( 
        <div style={style.container}>
            {props.characterList.map( character =>
                <CharacterStats
                    key = {character.id}
                    name = {character.name}
                    picture = {character.picture}
                    gender = {character.gender}
                    race = {character.race.name}
                    role = {character.role.name}
                    lvl = {character.lvl}
                    exp_to_next_lvl = {character.exp_to_next_lvl}
                    states = {character.states.length <= 0 ? 'aucune' : 
                        <div>
                            {character.states.map( (state, index) => 
                                <span key={index}>{state} </span>
                            )}  
                        </div>
                    }
                    current_exp = {character.current_exp}
                    max_health = {character.stats.max_health}
                    current_health = {character.stats.current_health}
                    max_mana = {character.stats.max_mana}
                    current_mana = {character.stats.current_mana}
                    attack_damages = {character.stats.attack_damages}
                    magic_damages = {character.stats.magic_damages}
                    armor = {character.stats.armor}
                    magic_resist = {character.stats.magic_resist}
                    move_speed = {character.stats.move_speed}
                    head = {character.equipments.head === EMPTY ? <span>vide</span> : <span>{character.equipments.head.name}</span> }
                    arms = {character.equipments.arms.name}
                    body = {character.equipments.body.name}
                    
                    footer = {<Button/>}
                />
            )}
        </div>
        
     );
}
 
export default CharacterList;