import React from 'react';

const StatBar = props => {
    const widthBar = (props.current_value * 100) / props.max_value;

    const style = {
        barContainer: {
            height: '10px',
            border: '1px solid black',
            borderRadius: '15px',
            padding: '2px',
        },
        bar: {
            backgroundColor: props.color,
            height: '100%',
            width: `${widthBar}%`,
            borderRadius: '15px',
        },
        label: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between'
        }
    }
    return ( 
        <>
            <div style={style.label}> 
                <span>{props.stat_label}</span>
                <span>{props.current_value} /  {props.max_value}</span>
            </div>
            <div style={style.barContainer}>
                <div style={style.bar}></div>
            </div>
        </>
     );
}
 
export default StatBar;