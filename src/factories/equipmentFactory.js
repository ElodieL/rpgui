import Equipment from "../models/Equipment"
import Stats from "../models/Stats";

export const createEquipment = (type, name, health, mana, attackDamages, magicDamages, armor, magicResist, moveSpeed, states, effects, abilities) => {
    const equipmentStats = new Stats(health, health, mana, mana, attackDamages, magicDamages, armor, magicResist, moveSpeed)
    const newEquipment = new Equipment(type, name, equipmentStats, states, effects, abilities);
    return newEquipment;
}