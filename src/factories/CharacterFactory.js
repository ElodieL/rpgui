import Character from '../models/Character';

export const createNewCharacter = (name, race, gender, role) => {
    let newCharacter = new Character();
    newCharacter.generateId();
    newCharacter.setName(name);
    newCharacter.setInitialPicture();
    newCharacter.setGender(gender);
    newCharacter.setRace(race);
    newCharacter.setRole(role);
    newCharacter.setStates([]);
    newCharacter.setLvl(1);
    newCharacter.setCurrentExp(0);
    newCharacter.setExpToNextLvl(100);
    newCharacter.updateExpToNextlvl()
    newCharacter.setStatsFromRace();
    newCharacter.setEquipmentStarterFromRole();
    console.log(newCharacter);
    return newCharacter;
}

export const createCharacter = (name, gender, race, role) => {
    let newCharacter = new Character();
    newCharacter.initialiseNewCharacter(name, gender, race, role);
    return newCharacter;
}
