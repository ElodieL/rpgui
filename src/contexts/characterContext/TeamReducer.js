export const ADD_CHARACTER = 'ADD_CHARACTER';
export const REMOVE_CHARACTER = 'REMOVE_CHARACTER';
export const LEVEL_UP = 'LEVEL_UP';

const addCharacter = (character, state) => {
    const updatedPlayerTeam = [...state.playerTeam];
    const updatedCharIndex = updatedPlayerTeam.findIndex(
      char => char.id === character.id
    );
    if (updatedCharIndex < 0) {
        updatedPlayerTeam.push({ ...character });
    };
    return {...state, playerTeam: updatedPlayerTeam};
};

const removeCharacter = (charId, state) => {
    const updatedPlayerTeam = [...state.playerTeam];
    const updatedCharIndex = updatedPlayerTeam.findIndex(
        char => char.id === charId
    );
        updatedPlayerTeam.splice(updatedCharIndex, 1);
    
    console.log(updatedPlayerTeam);
    return {...state, playerTeam: updatedPlayerTeam};
};

const levelUp = (charId, state) => {
    const updatedPlayerTeam = [...state.updatedPlayerTeam];
    const updatedCharIndex = updatedPlayerTeam.findIndex(
        char => char.id === charId.id
    );
    const updatedChar = {...updatedPlayerTeam[updatedCharIndex]};
    updatedChar.levelUp();
    updatedPlayerTeam[updatedCharIndex] = updatedChar;
    return {...state, playerTeam: updatedPlayerTeam};
}


export const TeamReducers = (state, action) => {
    switch(action.type) {
        case ADD_CHARACTER:
            return addCharacter(action.character, state);
        case REMOVE_CHARACTER:
            return removeCharacter(action.charId, state);
        case LEVEL_UP:
            return levelUp(action.charId, state);
        default:
            return state;
    }

}