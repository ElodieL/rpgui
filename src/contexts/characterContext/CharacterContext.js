import React, { createContext, useReducer } from 'react';
import * as characters_data from '../../data/characters';
import * as equipments_data from '../../data/equipments';
import { TeamReducers, ADD_CHARACTER, REMOVE_CHARACTER, LEVEL_UP } from './TeamReducer';

export const CharacterContext = createContext({});

export const CharacterState = props => {
    const all_equipments = equipments_data.all_equipments;
    const all_characters = characters_data.all_characters;
    const [teamState, dispatch] = useReducer(TeamReducers, { playerTeam: [], enemyTeam: [] });

    const addCharacter = character => {
        dispatch({type: ADD_CHARACTER, character: character})
    };
    const removeCharacter = charId => {
        dispatch({type: REMOVE_CHARACTER, charId: charId});
    };
    const levelUp = charId => {
        dispatch({type: LEVEL_UP, charId: charId})
    }



    return (
        <CharacterContext.Provider
            value = {{
                all_characters: all_characters,
                all_equipments: all_equipments,
                playerTeam: teamState.playerTeam,
                enemyTeam: teamState.enemyTeam,
                addCharacter: addCharacter,
                removeCharacter: removeCharacter,
                levelUp: levelUp,
            }}
        >
            {props.children}
        </CharacterContext.Provider>
    )
}