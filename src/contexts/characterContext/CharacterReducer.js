export const LEVEL_UP = 'LEVEL_UP';

const characterLevelUp = (character, state) => {
    return console.log('level up !')
}


export const CharacterReducer = (state, action) => {
    switch(action.type) {
        case LEVEL_UP:
            return characterLevelUp(action.character, state);
        default:
            return state;
    }

}